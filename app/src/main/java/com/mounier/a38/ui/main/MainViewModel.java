package com.mounier.a38.ui.main;

import android.content.Context;
import android.content.res.AssetManager;

import androidx.lifecycle.ViewModel;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mounier.a38.models.Motif;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainViewModel extends ViewModel {
    private Date currentTime;
    // TODO: Implement the ViewModel
    private String heureDeSortie;
    private String dateDeSortie;
    private List<Motif> motifs;

    public String getHeureDeSortie() {
        return customDateCreator(heureDeSortie, "HH:mm");
    }

    public void setHeureDeSortie(String heureDeSortie) {
        this.heureDeSortie = heureDeSortie;
    }

    public String getDateDeSortie() {
        return customDateCreator(dateDeSortie, "dd/MM/yyyy");
    }

    public void setDateDeSortie(String dateDeSortie) {
        this.dateDeSortie = dateDeSortie;
    }

    private String customDateCreator(String output, String format) {
        if (output == null) {
            if (currentTime == null) {
                currentTime = new Date();
            }
            output = new SimpleDateFormat(format).format(currentTime);
        }
        return output;
    }

    public List<Motif> getMotifs(Context context) throws IOException {
        if (motifs == null) {
            ObjectMapper objectMapper = new ObjectMapper();
            motifs = objectMapper.readValue(getAssetInString(context, "motifs.json"), objectMapper.getTypeFactory().constructCollectionType(List.class, Motif.class));
        }
        return motifs;
    }

    public void setMotifs(List<Motif> motifs) {
        this.motifs = motifs;
    }

    public String getAssetInString(Context context, String filename) throws IOException {
        AssetManager assetManager = context.getAssets();
        InputStream is = assetManager.open(filename);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        return new String(buffer, StandardCharsets.UTF_8);
    }
}
