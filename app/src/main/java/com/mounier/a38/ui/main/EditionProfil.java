package com.mounier.a38.ui.main;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mounier.a38.R;
import com.mounier.a38.models.Adresse;
import com.mounier.a38.models.Profil;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;


public class EditionProfil extends Fragment {

    Button btnValidate;
    EditText lblVoieEtNumero;
    EditText lblCodePostal;
    EditText lblCommune;
    EditText dateDeNaissance;
    EditText lieuDeNaissance;
    EditText lblPrenom;
    EditText lblNom;
    File file;
    Boolean formIsValide = false;

    boolean firstSlashInDateDisplayed = false;
    boolean secondSlashInDateDisplayed = false;

    ObjectMapper mapper = new ObjectMapper();

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_profil_view, container, false);

        btnValidate = view.findViewById(R.id.btnValidateEdition);
        lblVoieEtNumero = (EditText) view.findViewById(R.id.lblNumeroEtVoie);
        lblCodePostal = (EditText) view.findViewById(R.id.lblCodePostal);
        lblCommune = (EditText) view.findViewById(R.id.lblCommune);
        dateDeNaissance = (EditText) view.findViewById(R.id.lblNaissanceDate);
        lieuDeNaissance = (EditText) view.findViewById(R.id.lblNaissanceCommune);
        lblPrenom = (EditText) view.findViewById(R.id.lblPrenom);
        lblNom = (EditText) view.findViewById(R.id.lblNom);

        //Efface le champ quand on le clique pour l'éditer.
        setClearOnSelctForLbl(dateDeNaissance);
        setClearOnSelctForLbl(lblVoieEtNumero);
        setClearOnSelctForLbl(lblCodePostal);
        setClearOnSelctForLbl(lblCommune);
        setClearOnSelctForLbl(lieuDeNaissance);
        setClearOnSelctForLbl(lblPrenom);
        setClearOnSelctForLbl(lblNom);

        //Set les events listeners pour vérifier que le formulaire est valide
        setValidFormVerificationOnTextChangedListener(lblVoieEtNumero);
        setValidFormVerificationOnTextChangedListener(lblCodePostal);
        setValidFormVerificationOnTextChangedListener(lblCommune);
        setValidFormVerificationOnTextChangedListener(lieuDeNaissance);
        setValidFormVerificationOnTextChangedListener(lblPrenom);
        setValidFormVerificationOnTextChangedListener(lblNom);

        dateDeNaissance.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = dateDeNaissance.getText().toString();

                if (str.length() == 2) {
                    dateDeNaissance.setText(firstSlashInDateDisplayed ? str.subSequence(0, 1) : dateDeNaissance.getText().toString() + "/");
                    dateDeNaissance.setSelection(dateDeNaissance.getText().length());
                    firstSlashInDateDisplayed = !firstSlashInDateDisplayed;
                }
                if (str.length() == 5) {
                    dateDeNaissance.setText(secondSlashInDateDisplayed ? str.subSequence(0, 4) : dateDeNaissance.getText().toString() + "/");
                    dateDeNaissance.setSelection(dateDeNaissance.getText().length());
                    secondSlashInDateDisplayed = !secondSlashInDateDisplayed;
                }
                if (str.length() > 10) {
                    dateDeNaissance.setText(str.subSequence(0, 10));
                    dateDeNaissance.setSelection(dateDeNaissance.getText().length());
                }
            }

            public void afterTextChanged(Editable s) {
                if (formIsValide) {
                    btnValidate.setBackgroundColor(0xFF4CAF50);
                } else {
                    btnValidate.setBackgroundColor(0xFF405941);
                }
            }
        });


        lblCodePostal.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = lblCodePostal.getText().toString();
                if (str.length() > 5) {
                    lblCodePostal.setText(str.subSequence(0, 5));
                    lblCodePostal.setSelection(lblCodePostal.getText().length());
                }
            }

            public void afterTextChanged(Editable s) {

            }
        });

        file = new File(getContext().getFilesDir().getPath() + "/profil.json");
        if (file.exists()) {
            try {
                fillForm();
            } catch (IOException e) {
                Toast.makeText(getContext(), "Erreur : " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        btnValidate.setOnClickListener(v -> {
            if (formIsValide) {
                try {
                    saveProfilFile(v);
                } catch (ParseException | IOException e) {
                    e.printStackTrace();
                }
                NavHostFragment.findNavController(EditionProfil.this)
                        .navigate(R.id.enregistrement_action);
            } else {
                Toast.makeText(getContext(), "Veuillez remplir tous les champs", Toast.LENGTH_LONG).show();
            }

        });
        return view;
    }

    private void saveProfilFile(View view) throws ParseException, IOException {

        Adresse adresse = new Adresse(lblVoieEtNumero.getText().toString(), Integer.parseInt(lblCodePostal.getText().toString()), lblCommune.getText().toString());
        Profil profil = new Profil(lblNom.getText().toString(), lblPrenom.getText().toString(), dateDeNaissance.getText().toString(), lieuDeNaissance.getText().toString(), adresse);

        mapper.writeValue(file, profil);

    }


    private void fillForm() throws IOException {
        Profil profil = mapper.readValue(file, Profil.class);
        lblVoieEtNumero.setText(profil.getAdresse().getNumeroEtVoie());
        lblCodePostal.setText(String.format("%03d", profil.getAdresse().getCodePostal()));
        lblCommune.setText(profil.getAdresse().getCommune());
        dateDeNaissance.setText(profil.getDateDeNaissance());
        lieuDeNaissance.setText(profil.getLieuDeNaissance());
        lblPrenom.setText(profil.getPrenom());
        lblNom.setText(profil.getNom());

    }

    private void setClearOnSelctForLbl(EditText editText) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                firstSlashInDateDisplayed = false;
                secondSlashInDateDisplayed = false;
                if (hasFocus) {
                    editText.getText().clear();
                }
            }
        });
    }

    private void setValidFormVerificationOnTextChangedListener(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable s) {
                formIsValide = (dateDeNaissance.getText().toString().matches("^[0-9]{2}/[0-9]{2}/[0-9]{4}$"))
                        && (!lblVoieEtNumero.getText().toString().isEmpty())
                        && (!lblPrenom.getText().toString().isEmpty())
                        && (!lblCommune.getText().toString().isEmpty())
                        && !(lieuDeNaissance.getText().toString().isEmpty())
                        && !(lblNom.getText().toString().isEmpty())
                        && (lblCodePostal.getText().toString().matches("^[0-9]{4,5}$"));
                if (formIsValide) {
                    btnValidate.setBackgroundColor(0xFF4CAF50);
                } else {
                    btnValidate.setBackgroundColor(0xFF405941);
                }
            }
        });

    }

}
