package com.mounier.a38.ui.main;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mounier.a38.R;
import com.mounier.a38.models.GeneratePdf;
import com.mounier.a38.models.Motif;
import com.mounier.a38.models.Profil;
import com.tom_roush.pdfbox.util.PDFBoxResourceLoader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SelectionMotif extends Fragment {

    private List<Motif> motifs;
    private Profil profil;
    private Set<Motif> selectedMotifs = new TreeSet<Motif>((o1, o2) -> o2.getPosition() - o1.getPosition()) {
    };
    private Button btnValidate;
    private TextView heureDeSortie;
    private TextView dateDeSortie;
    private TimePickerDialog hourPicker;
    private DatePickerDialog datePicker;
    private MainViewModel mainViewModel;

    public static SelectionMotif newInstance() {
        return new SelectionMotif();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.selection_motif, container, false);
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        btnValidate = view.findViewById(R.id.btnValidate);
        btnValidate.setOnClickListener(v -> {
            selectedMotifs.clear();
            for (Motif motif : motifs) {
                if (motif.isSelected()) {
                    selectedMotifs.add(motif);
                }
            }
            generateNewAttestation(profil, selectedMotifs);
        });

        heureDeSortie = (TextView) view.findViewById(R.id.changerHeureDeSortie);
        dateDeSortie = (TextView) view.findViewById(R.id.changerDateDeSortie);

        dateDeSortie.setText(mainViewModel.getDateDeSortie());
        dateDeSortie.setOnClickListener(v -> {
            int day = Integer.parseInt(dateDeSortie.getText().toString().substring(0, 2));
            int month = Integer.parseInt(dateDeSortie.getText().toString().substring(3, 5));
            int year = Integer.parseInt(dateDeSortie.getText().toString().substring(6));
            // date picker dialog
            datePicker = new DatePickerDialog(getContext(),
                    (tp, sYear, sMonth, sDay) -> {
                        dateDeSortie.setText(String.format("%02d/%02d/%04d", sDay, sMonth + 1, sYear));
                        mainViewModel.setDateDeSortie(String.format("%02d/%02d/%04d", sDay, sMonth + 1, sYear));
                    }, year, month - 1, day);
            datePicker.show();
        });

        heureDeSortie.setText(mainViewModel.getHeureDeSortie());
        heureDeSortie.setOnClickListener(v -> {
            int hour = Integer.parseInt(heureDeSortie.getText().toString().substring(0, 2));
            int minutes = Integer.parseInt(heureDeSortie.getText().toString().substring(3));
            // time picker dialog
            hourPicker = new TimePickerDialog(getContext(),
                    (tp, sHour, sMinute) -> {
                        heureDeSortie.setText(String.format("%02d:%02d", sHour, sMinute));
                        mainViewModel.setHeureDeSortie(String.format("%02d:%02d", sHour, sMinute));
                    }, hour, minutes, true);
            hourPicker.show();
        });

        try {
            this.loadData();
            this.displayList(view);
        } catch (IOException e) {
            Toast.makeText(getContext(), "Erreur : " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return view;
    }


    public void loadData() throws IOException {
        PDFBoxResourceLoader.init(getContext());
        ObjectMapper objectMapper = new ObjectMapper();
        motifs = mainViewModel.getMotifs(getContext());
        profil = objectMapper.readValue(new File(getContext().getFilesDir().getPath() + "/profil.json"), Profil.class);

    }

    public void displayList(View view) {
        RecyclerView motifList = view.findViewById(R.id.list_motifs);

        List<Drawable> motifsIcons = new ArrayList<>();
        for (Motif motif : motifs) {
            Resources resources = getContext().getResources();
            int resourceId = resources.getIdentifier(motif.getIcon(), "drawable",
                    getContext().getPackageName());
            motifsIcons.add(ResourcesCompat.getDrawable(resources, resourceId, null));
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        MotifListAdapter motifListAdapter = new MotifListAdapter(motifs, motifsIcons, btnValidate);
        motifList.setAdapter(motifListAdapter);
        motifList.setLayoutManager(mLayoutManager);
        motifList.setItemAnimator(new DefaultItemAnimator());
        boolean oneOrMoreSelected = false;
        for (Motif motif : motifs) {
            if (motif.isSelected()) {
                oneOrMoreSelected = true;
            }
        }
        btnValidate.setEnabled(oneOrMoreSelected);
        btnValidate.setBackgroundColor(btnValidate.isEnabled() ? 0xFF4CAF50 : 0xFF405941);
    }

    public String getAssetInString(String filename) throws IOException {
        AssetManager assetManager = getContext().getAssets();
        InputStream is = assetManager.open(filename);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        return new String(buffer, StandardCharsets.UTF_8);
    }

    public void generateNewAttestation(Profil profil, Set<Motif> motifs) {
        btnValidate.setBackgroundColor(0xFF405941);
        btnValidate.setText("Génération en cours");
        btnValidate.setEnabled(false);
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    GeneratePdf.generateNewAttestation(profil, motifs, dateDeSortie.getText().toString(), heureDeSortie.getText().toString(), getContext());
                } catch (Exception e) {
                    Toast.makeText(getContext(), "Erreur : " + e.getClass().getName(), Toast.LENGTH_LONG).show();
                    Log.e("alpha", e.getMessage());
                } finally {
                    btnValidate.setEnabled(true);
                    btnValidate.setBackgroundColor(0xFF4CAF50);
                    btnValidate.setText("Générer");
                }
            }
        };

        handler.post(runnable);
    }

}
