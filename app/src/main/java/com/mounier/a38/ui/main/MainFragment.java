package com.mounier.a38.ui.main;

import android.Manifest;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.mounier.a38.R;

import java.io.File;

public class MainFragment extends Fragment {

    private ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    NavHostFragment.findNavController(MainFragment.this)
                            .navigate(R.id.selection_action);
                } else {
                    Toast.makeText(getContext(), "La permission est nécessaire pour enregistrer l'attestation dans vos documents.", Toast.LENGTH_LONG).show();
                }
            });

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_fragment, container, false);
        Button btnGenerate = view.findViewById(R.id.btnGenerate);
        btnGenerate.setOnClickListener(v -> requestPermissionLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE));
        Button btnEdit = view.findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(v -> NavHostFragment.findNavController(MainFragment.this)
                .navigate(R.id.edition_action));
        File profilFile = new File(getContext().getFilesDir().getPath() + "/profil.json");
        if (profilFile.exists()) {
            btnGenerate.setEnabled(true);
            btnGenerate.setBackgroundColor(0xFF4CAF50);
            btnEdit.setText("Editer le profil");
        }

        return view;

    }

}
