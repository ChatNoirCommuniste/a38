package com.mounier.a38.ui.main;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mounier.a38.R;
import com.mounier.a38.models.Motif;

import java.util.List;

public class MotifListAdapter extends RecyclerView.Adapter<MotifListAdapter.MotifListViewHolder> {

    private List<Motif> motifs;
    private List<Drawable> motifIcons;
    private Button btnValider;

    public MotifListAdapter(List<Motif> motifs, List<Drawable> motifIcons, Button btnValider) {
        this.motifs = motifs;
        this.motifIcons = motifIcons;
        this.btnValider = btnValider;
    }


    @NonNull
    @Override
    public MotifListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View motifView = layoutInflater.inflate(R.layout.simple_list_single_choice, parent, false);
        return new MotifListViewHolder(motifView);
    }

    @Override
    public void onBindViewHolder(@NonNull MotifListViewHolder holder, int position) {
        holder.motifLabel.setText(motifs.get(position).getLabel());
        holder.motifIcon.setImageDrawable(motifIcons.get(position));
        holder.motifSelection.setChecked(motifs.get(position).isSelected());
    }

    @Override
    public int getItemCount() {
        return motifs.size();
    }


    public class MotifListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView motifLabel;
        ImageView motifIcon;
        CheckBox motifSelection;

        public MotifListViewHolder(@NonNull View itemView) {
            super(itemView);
            this.motifIcon = (ImageView) itemView.findViewById(R.id.iconMotif);
            this.motifLabel = (TextView) itemView.findViewById(R.id.textMotif);
            this.motifSelection = (CheckBox) itemView.findViewById(R.id.selectedMotif);
            itemView.setOnClickListener(this);
            motifSelection.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (!motifs.get(position).isSelected()) {
                motifs.get(position).setSelected(true);
                this.motifSelection.setChecked(true);
            } else {
                motifs.get(position).setSelected(false);
                this.motifSelection.setChecked(false);
            }
            editValidateEnabled();
        }

        public void editValidateEnabled() {
            boolean oneOrMoreSelected = false;
            for (Motif motif : motifs) {
                if (motif.isSelected()) {
                    oneOrMoreSelected = true;
                }
            }
            btnValider.setEnabled(oneOrMoreSelected);
            btnValider.setBackgroundColor(btnValider.isEnabled() ? 0xFF4CAF50 : 0xFF405941);
        }
    }

}
