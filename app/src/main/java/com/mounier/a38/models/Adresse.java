package com.mounier.a38.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Adresse {

    private String numeroEtVoie;
    private int codePostal;
    private String commune;

    public Adresse(String numeroEtVoie, int codePostal, String commune) {
        this.numeroEtVoie = numeroEtVoie;
        this.codePostal = codePostal;
        this.commune = commune;
    }

    public Adresse() {
    }

    @JsonIgnore
    public String getFullAdress() {
        return numeroEtVoie + " " + codePostal + " " + commune;
    }

    public String getNumeroEtVoie() {
        return numeroEtVoie;
    }

    public void setNumeroEtVoie(String numeroEtVoie) {
        this.numeroEtVoie = numeroEtVoie;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    @Override
    public String toString() {
        return "Adresse{" +
                "numeroEtVoie='" + numeroEtVoie + '\'' +
                ", codePostal=" + codePostal +
                ", commune='" + commune + '\'' +
                '}';
    }
}
