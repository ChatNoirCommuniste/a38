package com.mounier.a38.models;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;

import androidx.core.content.FileProvider;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.pdmodel.PDPage;
import com.tom_roush.pdfbox.pdmodel.PDPageContentStream;
import com.tom_roush.pdfbox.pdmodel.common.PDRectangle;
import com.tom_roush.pdfbox.pdmodel.font.PDFont;
import com.tom_roush.pdfbox.pdmodel.font.PDType1Font;
import com.tom_roush.pdfbox.pdmodel.graphics.image.LosslessFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class GeneratePdf {

    public static void generateNewAttestation(Profil profil, Set<Motif> motifs, String dateDeSortie, String heureDeSortie, Context context) throws WriterException, IOException {
        String qrCodeString = generateQRCodeString(profil, motifs, dateDeSortie, heureDeSortie, context);
        generatePDF(generateQRCodeBitmap(qrCodeString), profil, motifs, dateDeSortie, heureDeSortie, context);
    }

    private static String generateQRCodeString(Profil profil, Set<Motif> motifs, String dateDeSortie, String heureDeSortie, Context context) {
        StringBuilder codes = new StringBuilder();
        Iterator<Motif> motifIterator = motifs.iterator();
        while (motifIterator.hasNext()) {
            codes.append(motifIterator.next().getCode());
            if (motifIterator.hasNext()) {
                codes.append(", ");
            }
        }


        return "Cree le: " + dateDeSortie + " a " + heureDeSortie.replace(":", "h") + ";\n" +
                "Nom: " + profil.getNom() + ";\n" +
                "Prenom: " + profil.getPrenom() + ";\n" +
                "Naissance: " + profil.getDateDeNaissance() + " a " + profil.getLieuDeNaissance() + ";\n" +
                "Adresse: " + profil.getAdresse().getNumeroEtVoie() + " " + profil.getAdresse().getCodePostal() + " " + profil.getAdresse().getCommune() + ";\n" +
                "Sortie: " + dateDeSortie + " a " + heureDeSortie + ";\n" +
                "Motifs: " + codes + ";\n";
    }

    private static Bitmap generateQRCodeBitmap(String donneesAEncoder) throws WriterException {

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix bitMatrix = qrCodeWriter.encode(donneesAEncoder, BarcodeFormat.QR_CODE, 300, 300, hints);

        int height = bitMatrix.getHeight();
        int width = bitMatrix.getWidth();
        final Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
            }
        }
        return bmp;
    }

    private static void generatePDF(Bitmap bmp, Profil profil, Set<Motif> motifs, String dateDeSortie, String heureDeSortie, Context context) throws IOException {
        PDDocument pdDocument = PDDocument.load(context.getAssets().open("certificate.pdf"));
        String[] dateTabEntree = dateDeSortie.split("/");
        String fileName = "attestation-" + dateTabEntree[2] + "-" + dateTabEntree[1] + "-" + dateTabEntree[0] + "_" + heureDeSortie.replace(":", "-") + ".pdf";
        File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Download");
        File file;
        boolean isPresent = true;
        if (!docsFolder.exists()) {
            isPresent = docsFolder.mkdir();
        }
        if (isPresent) {
            file = new File(docsFolder.getAbsolutePath(), fileName);
        } else {
            throw new IOException();
        }

        PDPage pdPage = pdDocument.getPage(0);
        PDFont font = PDType1Font.HELVETICA;
        PDPageContentStream contentStream = new PDPageContentStream(pdDocument, pdPage, true, false);
        drawText(contentStream, font, 11, 92, 702, profil.getPrenom() + " " + profil.getNom());
        drawText(contentStream, font, 11, 92, 684, profil.getDateDeNaissance());
        drawText(contentStream, font, 11, 214, 684, profil.getLieuDeNaissance());
        drawText(contentStream, font, 11, 104, 665, profil.getAdresse().getFullAdress());

        for (Motif motif : motifs) {
            drawText(contentStream, font, 12, 47, motif.getPosition(), "x");
        }

        drawText(contentStream, font, 11, 78, 76, profil.getAdresse().getCommune());
        drawText(contentStream, font, 11, 63, 58, dateDeSortie);
        drawText(contentStream, font, 11, 227, 58, heureDeSortie);

        contentStream.drawImage(LosslessFactory.createFromImage(pdDocument, bmp), pdPage.getMediaBox().getWidth() - 156, 25, 92, 92);
        contentStream.close();

        PDPage pdPage2 = new PDPage(PDRectangle.A4);
        contentStream = new PDPageContentStream(pdDocument, pdPage2, true, false);
        contentStream.drawImage(LosslessFactory.createFromImage(pdDocument, bmp), 50, pdPage2.getMediaBox().getHeight() - 390, 300, 300);
        contentStream.close();
        pdDocument.addPage(pdPage2);
        pdDocument.save(file);
        pdDocument.close();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file), "application/pdf");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.startActivity(intent);
    }

    private void writeBitmapInFile(Bitmap bmp, Context context) throws FileNotFoundException {
        File qrcodeFichier = new File(context.getCacheDir().getPath(), "qrcode.png");
        FileOutputStream qrCodeOutput = new FileOutputStream(qrcodeFichier);
        bmp.compress(Bitmap.CompressFormat.PNG, 100, qrCodeOutput);
    }

    private static void drawText(PDPageContentStream contentStream, PDFont font, int textSize, int tx, int ty, String content) throws IOException {
        contentStream.beginText();
        contentStream.setNonStrokingColor(0);
        contentStream.setFont(font, textSize);
        contentStream.newLineAtOffset(tx, ty);
        contentStream.showText(content);
        contentStream.endText();
    }
}
