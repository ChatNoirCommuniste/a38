package com.mounier.a38.models;

import java.util.Objects;

public class Motif {
    private String code;
    private String label;
    private int position;
    private String icon;
    private Boolean selected = false;

    public Motif(String code, String label, int position, String icon) {
        this.code = code;
        this.label = label;
        this.position = position;
        this.icon = icon;
        this.selected = false;
    }

    public Motif() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean isSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Motif motif = (Motif) o;
        return position == motif.position &&
                code.equals(motif.code) &&
                label.equals(motif.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, label, position);
    }
}
