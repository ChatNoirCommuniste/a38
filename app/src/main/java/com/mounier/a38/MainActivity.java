package com.mounier.a38;

import android.Manifest;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.mounier.a38.ui.main.MainViewModel;

public class MainActivity extends AppCompatActivity {

    private ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    setContentView(R.layout.main_activity);
                } else {
                    new AlertDialog.Builder(this) // Pass a reference to your main activity here
                            .setTitle("Permission requise")
                            .setMessage("La permission est nécessaire pour enregistrer l'attestation dans vos documents.")
                            .setPositiveButton("OK", (dialogInterface, i) -> this.finish())
                            .show();

                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestPermissionLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE);

    }


}
