# README - A38 #

Documentation pour la V1.0 de l'application A38

### A quoi sert l'application A38? ###

L'application sert à  générer en quelques secondes, une attestation dérogatoire de déplacement dans le cadre du confinement de Novembre 2020.
Il suffit de choisir un motif de sortie (selon la liste officielle disponible au 09/11/2020) et, le cas échéant, l'heure de sortie. Une attestation PDF sera alors générée avec le QR Code lisible par la police, enregistrée dans le dossier documents de votre téléphone et ensuite ouvert dans le lecteur PDF de votre téléphone.
Aucune donnée n'est envoyé sur internet et l'application requiert uniquement l'accès au stockage pour pouvoir enregistrer les attestations dans le dossier document. Le code source est à la disposition sur ce dépôt.*
L'application est compatible avec Android 5.0 et supérieure.

### Comment démarrer ###

Vous devez autoriser l'installation d'applications venant de sources inconnues sur votre téléphone (pour votre sécurité, pensez à remodifier ce paramètre après avoir installé A38). Au premier démarrage, il vous sera nécéssaire l'accès au stockage et de créer le profil (toutes les informations à mettre sur une attestation officielle).

### Ressources externes utilisées ###

- La librairie PdfBox-Android de Tom Roush, basé sur le projet PDFBox d'Apache, utilisée dans la génération de PDF ( https://github.com/TomRoush/PdfBox-Android ).
- La librairie Jackson de FasterXML, LLC, utilisée dans le paramétrage en fichier JSON de l'application ( https://github.com/FasterXML/jackson ).
- La librairie ZXING de Google et de la communauté, utilisée dans la génération du QRCode ( https://github.com/zxing/zxing ).
- Le code source du générateur Web officiel du ministère de l'Intérieur ( https://github.com/LAB-MI/attestation-deplacement-derogatoire-q4-2020) pour le paramétrage du QRCode et du PDF selon le format officiel. 
- Evidemment <3 sur la communauté StackOverflow.
- Les Douze Travaux d'Astérix de René Goscinny et Albert Uderzo pour le nom de l'application et l'administration Française pour sa traditionnelle absurdité.

### Problèmes connues ###

* Génération du PDF lente, un correctif est prévu.

### Fonctionalités prévues ###

* Widget sur l'écran d'accueil pour afficher le dernier QRCode et générer une attestation en un clic.
* Gestion de plusieurs profils simultanées (enfants, partenaire, personnes-agées).
* Gestion, partage et affichage des attestations précédentes directement dans l'application (fonction assurée par le lecteur PDF du téléphone).
* Affichage du QRCode seul